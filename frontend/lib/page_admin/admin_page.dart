import 'package:flutter/material.dart';
import 'package:frontend/components/buttons.dart';

class AdminPage extends StatefulWidget {
  const AdminPage({Key? key}) : super(key: key);

  @override
  _AdminPageState createState() => _AdminPageState();
}

class _AdminPageState extends State<AdminPage> {
  int _id = 0;
  static const double WIDTH = 1030;

  Widget _buildOverlay() {
    return SizedBox(
      width: double.infinity,
      child: Column(
        children: [
          Container(
            width: 1030,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 29),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.min,
                children: [
                  AdminTabButton(
                    text: "Команда",
                    onTap: () {
                      setState(() {
                        _id = 0;
                      });
                    },
                  ),
                  AdminTabButton(
                    text: "Карта",
                    onTap: () {
                      setState(() {
                        _id = 1;
                      });
                    },
                  ),
                  AdminTabButton(
                    text: "Успехи",
                    onTap: () {
                      setState(() {
                        _id = 2;
                      });
                    },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildCard(Widget? child) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 23),
      child: Container(
        width: 492,
        height: 252,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(40),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.1),
              spreadRadius: 0,
              blurRadius: 20,
            ),
          ],
        ),
        child: child,
      ),
    );
  }

  Widget _buildTeam() {
    return Center(
      child: SingleChildScrollView(
        child: SizedBox(
          width: WIDTH,
          child: SizedBox(
            //height: MediaQuery.of(context).size.height,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 141,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Команда "Underground"',
                    style: TextStyle(
                      fontSize: 29,
                      color: Color(0xFF192AA7),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    _buildCard(Placeholder()),
                    _buildCard(Placeholder()),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    _buildCard(Placeholder()),
                    _buildCard(Placeholder()),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildProgress() {
    return Center(
      child: SingleChildScrollView(
        child: SizedBox(
          width: WIDTH,
          child: SizedBox(
            //height: MediaQuery.of(context).size.height,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 141,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Успехи',
                    style: TextStyle(
                      fontSize: 29,
                      color: Color(0xFF192AA7),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    _buildCard(Placeholder()),
                    _buildCard(Placeholder()),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    _buildCard(Placeholder()),
                    _buildCard(Placeholder()),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildBackground() {
    return Image.asset(
      "assets/images/appBackground.png",
      fit: BoxFit.cover,
      width: 14400,
      height: 8390,
    );
  }

  Widget _buildMap() {
    return Image.asset(
      "assets/images/map.png",
      fit: BoxFit.cover,
      width: 14400,
      height: 8390,
    );
  }

  Widget _buildContent() {
    switch (_id) {
      case 0:
        return _buildTeam();
      case 1:
        return _buildMap();
      case 2:
        return _buildProgress();
      default:
        return Placeholder();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          _buildBackground(),
          _buildContent(),
          _buildOverlay(),
        ],
      ),
    );
  }
}

//test

void main(List<String> args) {
  runApp(
    MaterialApp(
      title: 'Balance Platform',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: AdminPage(),
    ),
  );
}

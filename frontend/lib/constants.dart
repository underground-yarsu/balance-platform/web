import 'package:flutter/material.dart';

/// Константы
Color myColor = const Color(0xFFECECEC);

Color button = const Color(0xFF041BBA).withOpacity(0.6);
Color btnText = const Color(0xFF6274EE);

Color text = const Color(0xFF222B6B);
Color semiText = text.withOpacity(0.5);

Color profileBackground = const Color(0xFFF1F0F5);

Color blueText = const Color(0xFF192AA7);
Color indicator = const Color(0xFF3633E0).withOpacity(0.3);
Color white = const Color(0xFFFFFFFF);

Color shadow = const Color(0xFF000000).withOpacity(0.1);
Color shadowBtn = const Color(0xFF4200F6).withOpacity(0.05);

import 'package:flutter/material.dart';
import 'package:frontend/components/battleCard.dart';

import '../constants.dart';

class RightBar extends StatefulWidget {
  @override
  _RightBarState createState() => _RightBarState();
}

class _RightBarState extends State<RightBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 340,
      height: MediaQuery.of(context).size.height,
      color: white,
      alignment: Alignment.center,
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            for (int i = 1; i < 11; i++)
              BattleCard(
                firstGroup: 'Команда 1',
                secondGroup: 'Команда 2',
                avatar: i.toString(),
              ),
          ],
        ),
      ),
    );
  }
}

import 'dart:ui';

import 'package:flutter/material.dart';

import '../constants.dart';

/// Заглавный виджет авторизации пользователя
class AuthPage extends StatelessWidget {
  const AuthPage({
    required this.child,
    required this.width,
    required this.height,
    Key? key,
  }) : super(key: key);
  final Widget child;
  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Image.asset(
          'assets/images/authBackground.png',
          fit: BoxFit.cover,
          width: 10000,
          height: 10000,
        ),
        Center(
          child: ClipRRect(
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 15.0, sigmaY: 15.0),
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(width: 1, color: white),
                  boxShadow: [BoxShadow(color: shadow, blurRadius: 20)],
                  color: white.withOpacity(0.7),
                  borderRadius: BorderRadius.circular(20),
                ),
                padding: EdgeInsets.symmetric(horizontal: 50),
                height: height,
                width: width,
                child: Container(child: child),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

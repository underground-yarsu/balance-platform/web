import 'package:flutter/material.dart';
import 'package:frontend/api/client.dart';
import 'package:frontend/api/schema/profile.dart';
import 'package:frontend/constants.dart';
import 'package:frontend/global.dart';
import 'package:websafe_svg/websafe_svg.dart';

/// Верхняя панель пользователя
class ProfileAppBar extends StatefulWidget {
  @override
  _ProfileAppBarState createState() => _ProfileAppBarState();
}

class _ProfileAppBarState extends State<ProfileAppBar> {
  Profile? _profile;

  @override
  void initState() {
    super.initState();
    client.getProfile(Prefs.telegram!).then((Profile value) {
      setState(() {
        _profile = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_profile == null) {
      return Container(
        width: 600,
        height: 100,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(40)),
          boxShadow: [BoxShadow(color: shadow, blurRadius: 15)],
          color: white,
        ),
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 50),
        alignment: Alignment.center,
      );
    }
    return Container(
      width: 600,
      height: 100,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(40)),
        boxShadow: [BoxShadow(color: shadow, blurRadius: 15)],
        color: white,
      ),
      padding: EdgeInsets.symmetric(vertical: 20, horizontal: 50),
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            flex: 1,
            child: Text(
              '65%',
              style: TextStyle(color: blueText, fontSize: 18),
            ),
          ),
          Expanded(
            flex: 8,
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 50),
              height: 15,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  border: Border.all(color: blueText)),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                child: LinearProgressIndicator(
                  value: 0.7,
                  backgroundColor: white,
                  valueColor: AlwaysStoppedAnimation<Color>(blueText),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Row(
              children: [
                WebsafeSvg.asset('icons/coins.svg'),
                Text(
                  (_profile!.balance ?? 0).toString(),
                  style: TextStyle(color: blueText, fontSize: 18),
                )
              ],
            ),
          ),
          Expanded(
            flex: 2,
            child: WebsafeSvg.asset('icons/avatar.svg'),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:frontend/components/buttons.dart';
import 'package:frontend/components/text_field.dart';
import 'package:frontend/constants.dart';
import 'package:websafe_svg/websafe_svg.dart';

/// Создать соревнование
class BattleDialog extends StatefulWidget {
  BattleDialog(BuildContext context);

  @override
  _BattleDialogState createState() => _BattleDialogState();
}

class _BattleDialogState extends State<BattleDialog> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30),
      ),
      backgroundColor: white,
      child: Container(
        width: 540,
        height: 360,
        padding: EdgeInsets.symmetric(horizontal: 30),
        child: Column(
          children: [
            Row(
              children: [
                Spacer(),
                IconButton(
                  onPressed: () => Navigator.of(context).pop(),
                  icon: Icon(Icons.cancel_outlined),
                  color: blueText,
                  iconSize: 20,
                ),
              ],
            ),
            Container(
              height: 310,
              width: 480,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    height: 20,
                    child: Row(
                      children: [
                        Text('Условие',
                            style: TextStyle(color: blueText, fontSize: 22)),
                        Icon(Icons.home)
                      ],
                    ),
                  ),
                  textField('Введите условие'),
                  Row(
                    children: [
                      Text('Ставка',
                          style: TextStyle(color: blueText, fontSize: 22)),
                      WebsafeSvg.asset('icons/coins.svg'),
                    ],
                  ),
                  textField('Введите сумму'),
                  Container(
                    width: 330,
                    child: MyButton(
                      text: 'Создать',
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget textField(String hintText) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: double.infinity,
            padding: EdgeInsets.only(left: 16),
            decoration: BoxDecoration(
              color: Color(0xFFFFFFFF),
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: shadowBtn,
                  blurRadius: 5,
                  spreadRadius: 5,
                )
              ],
            ),
            child: Row(
              children: [
                SizedBox(width: 13.2),
                Container(
                  width: 250,
                  child: TextFormField(
                    cursorColor: text,
                    decoration: InputDecoration(
                      fillColor: Colors.red,
                      border: InputBorder.none,
                      hintText: hintText,
                      hintStyle: TextStyle(
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                          fontSize: 18,
                          color: semiText),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      );
}

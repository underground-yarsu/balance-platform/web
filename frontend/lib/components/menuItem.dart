import 'package:flutter/material.dart';
import 'package:frontend/constants.dart';
import 'package:websafe_svg/websafe_svg.dart';

// Компонент левой навигационной панели
class MenuItes extends StatelessWidget {
  int index;
  MenuItes({required this.index});
  final menuItemsIcons = [
    'tasks',
    'map',
    'statistic',
    'tips',
    'rating',
    'bets',
    'shop',
  ];
  final menuItems = [
    'Задачи',
    'Карта',
    'Статистика',
    'Советы',
    'Рейтинг',
    'Ставки',
    'Магазин',
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: 30,
            height: 30,
            child:
                WebsafeSvg.asset('icons/menuItem/${menuItemsIcons[index]}.svg'),
          ),
          Container(
            padding: EdgeInsets.only(left: 20),
            child: Text(
              menuItems[index],
              style: TextStyle(color: blueText, fontSize: 18),
            ),
          ),
        ],
      ),
    );
  }
}

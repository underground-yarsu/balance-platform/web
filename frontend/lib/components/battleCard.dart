import 'package:flutter/material.dart';
import 'package:frontend/constants.dart';
import 'package:websafe_svg/websafe_svg.dart';

/// Карточка соревнования
class BattleCard extends StatelessWidget {
  final String firstGroup;
  final String secondGroup;
  final String avatar;
  BattleCard(
      {required this.firstGroup,
      required this.secondGroup,
      required this.avatar});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 15, left: 20, right: 20),
      width: 300,
      height: 190,
      decoration: BoxDecoration(
          color: white,
          borderRadius: BorderRadius.circular(30),
          boxShadow: [BoxShadow(color: shadow, blurRadius: 15)]),
      child: Container(
          padding: EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Image.asset('avatars/$avatar.png'),
                  Text(
                    firstGroup,
                    style: TextStyle(color: blueText, fontSize: 18),
                  ),
                  WebsafeSvg.asset('icons/plusBtn.svg'),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    'VS',
                    style: TextStyle(color: blueText, fontSize: 24),
                  ),
                  Container()
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  CircleAvatar(backgroundColor: blueText, radius: 35),
                  Text(
                    secondGroup,
                    style: TextStyle(color: blueText, fontSize: 18),
                  ),
                  WebsafeSvg.asset('icons/plusBtn.svg'),
                ],
              ),
            ],
          )),
    );
  }
}

import 'package:flutter/material.dart';

import '../constants.dart';

/// Индикатор загрузки
class Indicator {
  static Widget circle = Center(
      child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(
    myColor,
  )));
}

import 'package:dio/dio.dart';
import 'package:frontend/global.dart';

class ApiInterceptor extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    if (!['/reg', '/login'].contains(options.path))
      options.headers['Authorization'] = 'Bearer ${Prefs.jwt}';
    if (options.data != null)
      options.headers['Content-Type'] = 'application/json';
    super.onRequest(options, handler);
  }
}
import 'package:json_annotation/json_annotation.dart';

part 'reg_request.g.dart';

@JsonSerializable(createFactory: false)
class RegRequest {
  RegRequest(
      {required this.fio, required this.telegramName, required this.password});

  @JsonKey(name: 'fio')
  String fio;
  @JsonKey(name: 'telegram_name')
  String telegramName;
  @JsonKey(name: 'password')
  String password;

  Map<String, dynamic> toJson() => _$RegRequestToJson(this);
}

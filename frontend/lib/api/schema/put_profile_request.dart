import 'package:json_annotation/json_annotation.dart';

part 'put_profile_request.g.dart';

@JsonSerializable(createFactory: false)
class PutProfileRequest {
  PutProfileRequest({
    required this.fio,
    required this.id_team,
  });

  String fio;
  int id_team;

  Map<String, dynamic> toJson() => _$PutProfileRequestToJson(this);
}

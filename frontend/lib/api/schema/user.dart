import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  User({
    required this.balance,
    this.img,
    required this.team_name,
    required this.telegram_name,
  });

  int balance;
  String? img;
  String team_name;
  String telegram_name;

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}

import 'package:json_annotation/json_annotation.dart';

part 'team_info.g.dart';

@JsonSerializable(createToJson: false)
class TeamInfo {
  TeamInfo({
    required this.sum,
    required this.team_name,
  });

  int sum;
  String team_name;

  factory TeamInfo.fromJson(Map<String, dynamic> json) =>
      _$TeamInfoFromJson(json);
}

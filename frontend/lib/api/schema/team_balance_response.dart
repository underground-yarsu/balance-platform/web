import 'package:json_annotation/json_annotation.dart';

part 'team_balance_response.g.dart';

@JsonSerializable(createToJson: false)
class TeamBalanceResponse {
  TeamBalanceResponse({required this.sum});
  final int sum;

  factory TeamBalanceResponse.fromJson(Map<String, dynamic> json) =>
      _$TeamBalanceResponseFromJson(json);
}

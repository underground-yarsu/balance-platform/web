import 'package:json_annotation/json_annotation.dart';

part 'achieve_user.g.dart';

@JsonSerializable()
class AchieveUser {
  AchieveUser({
    required this.ach_price,
    required this.id_ach,
    required this.name_ach,
  });

  int ach_price;
  int id_ach;
  String name_ach;

  factory AchieveUser.fromJson(Map<String, dynamic> json) =>
      _$AchieveUserFromJson(json);
  Map<String, dynamic> toJson() => _$AchieveUserToJson(this);
}

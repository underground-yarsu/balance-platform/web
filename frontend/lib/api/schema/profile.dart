import 'package:frontend/api/schema/achievement.dart';
import 'package:json_annotation/json_annotation.dart';

part 'profile.g.dart';

@JsonSerializable(createToJson: false)
class Profile {
  Profile({
    required this.achivements,
    required this.balance,
    required this.fio,
    required this.img,
    required this.telegramName,
    required this.teamName,
  });

  List<Achievement> achivements;
  @JsonKey(name: 'balance')
  int? balance;
  @JsonKey(name: 'fio')
  String fio;
  @JsonKey(name: 'img')
  String? img;
  @JsonKey(name: 'team_name')
  int? teamName;
  @JsonKey(name: 'telegram_name')
  String telegramName;

  factory Profile.fromJson(Map<String, dynamic> json) =>
      _$ProfileFromJson(json);
}

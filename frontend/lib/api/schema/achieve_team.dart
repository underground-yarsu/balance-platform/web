import 'package:json_annotation/json_annotation.dart';

part 'achieve_team.g.dart';

@JsonSerializable()
class AchieveTeam {
  AchieveTeam({
    required this.ach_price,
    required this.name_ach_team,
  });

  String name_ach_team;
  int ach_price;

  factory AchieveTeam.fromJson(Map<String, dynamic> json) =>
      _$AchieveTeamFromJson(json);
  Map<String, dynamic> toJson() => _$AchieveTeamToJson(this);
}

import 'package:json_annotation/json_annotation.dart';

part 'access_token.g.dart';

@JsonSerializable(createToJson: false)
class AccessToken {
  AccessToken({required this.accessToken});

  @JsonKey(name: 'access_token')
  String accessToken;

  factory AccessToken.fromJson(Map<String, dynamic> json) =>
      _$AccessTokenFromJson(json);
}

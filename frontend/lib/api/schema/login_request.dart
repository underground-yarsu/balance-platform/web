import 'package:json_annotation/json_annotation.dart';

part 'login_request.g.dart';

@JsonSerializable(createFactory: false)
class LoginRequest {
  LoginRequest({required this.telegramName, required this.password});

  @JsonKey(name: 'telegram_name')
  String telegramName;
  @JsonKey(name: 'password')
  String password;

  Map<String, dynamic> toJson() => _$LoginRequestToJson(this);
}

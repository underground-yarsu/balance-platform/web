import 'package:json_annotation/json_annotation.dart';

part 'protected_schema.g.dart';

@JsonSerializable(createToJson: false)
class ProtectedSchema {
  ProtectedSchema({required this.loggedInAs});

  @JsonKey(name: 'logged_in_as')
  String loggedInAs;

  factory ProtectedSchema.fromJson(Map<String, dynamic> json) =>
      _$ProtectedSchemaFromJson(json);
}

import 'package:dio/dio.dart';
import 'package:frontend/api/client.dart';
import 'package:frontend/api/interceptor.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Global {
  static Future<void> init() async {
    client = ApiClient(dio, baseUrl: uri.toString());
    Prefs.prefs = await SharedPreferences.getInstance();
  }

  // Network
  static Uri get uri => Uri.parse('https://back-hack.herokuapp.com/');
  static const int connectTimeout = 5000; /* milliseconds */
  static const int receiveTimeout = 5000; /* milliseconds */
  static final Dio dio = new Dio()
    ..interceptors.add(ApiInterceptor())
    ..options.connectTimeout = connectTimeout
    ..options.receiveTimeout = receiveTimeout
    ..httpClientAdapter;
}

class Prefs {
  static late SharedPreferences prefs;

  // example:

  // static String? get myField => prefs.getString('myField');
  // static set myField(String? value) => value != null
  //     ? prefs.setString('myField', value)
  //     : prefs.remove('myField');

  static String? get jwt => prefs.getString('jwt');
  static set jwt(String? value) {
    if (value != null)
      prefs.setString('jwt', value);
    else
      prefs.remove('jwt');
  }

  static String? get telegram => prefs.getString('telegram');
  static set telegram(String? value) {
    if (value != null)
      prefs.setString('telegram', value);
    else
      prefs.remove('telegram');
  }
}

import 'package:flutter/material.dart';
import 'package:frontend/constants.dart';
import 'package:frontend/page/rating/myRatingCard.dart';

class RatingPage extends StatefulWidget {
  @override
  _RatingPageState createState() => _RatingPageState();
}

class _RatingPageState extends State<RatingPage> {
  /// Вкладка рейтинг
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Container(
        padding: EdgeInsets.only(
          left: 84,
          right: 76
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Мой рейтинг',
              style: TextStyle(color: blueText, fontSize: 28),
            ),
            MyRating(),
            Text(
              'Общий рейтинг',
              style: TextStyle(color: blueText, fontSize: 28),
            ),
            SizedBox(height: 16),
            Row(
              children: [
                Container(
                  padding: EdgeInsets.only(
            top: 30,
                left: 27,
              right: 21
            ),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(40),
                    boxShadow: [BoxShadow(color: Color.fromRGBO(27, 11, 71, 0.1))]
                  ),
                  child: Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(width: 3),
                          Image.asset(
                            "assets/avatars/Nikita.png",
                            height: 100,
                            width: 100
                          ),
                          SizedBox(width: 25),
                          Column(
                            children: [
                              Text(
                                "Никита Аверкин",
                                style: TextStyle(
                                  color: Color(0xff192AA7),
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500
                                ),
                              ),
                              SizedBox(height: 13),
                              Row(
                                children: [
                                  Image.asset(
                                    "assets/trophy.png"
                                  ),
                                  SizedBox(width: 10),
                                  Text(
                                    "93 уровень",
                                    style: TextStyle(
                                      color: Color(0xffFFC250),
                                      fontWeight: FontWeight.w500,
                                      fontSize: 5
                                    ),
                                  )
                                ],
                              )
                            ],
                          )
                        ],
                      ),
                      SizedBox(height: 20),
                      Row(
                        children: [
                          Container(
                            height: 90,
                            width: 104,
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              color: Color(0xffFDFDF1),
                              borderRadius: BorderRadius.circular(10)
                            ),
                            child: Column(
                              children: [
                                Image.asset(
                                  "images/cards/in_the_last_moment.png",
                                  height: 30,
                                  width: 30,
                                ),
                                SizedBox(height: 5),
                                Text(
                                    "В последний момент",
                                  style: TextStyle(
                                    color: Color(0xffFF7337),
                                    fontWeight: FontWeight.w500,
                                    fontSize: 8
                                  ),
                                ),
                                SizedBox(height: 5),
                                Text(
                                  "В последний момент",
                                  style: TextStyle(
                                      color: Color.fromRGBO(255, 115, 55, 0.5),
                                      fontWeight: FontWeight.w500,
                                      fontSize: 6
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(width: 10),
                          Container(
                            height: 90,
                            width: 104,
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              color: Color(0xffFDF0FB),
                                borderRadius: BorderRadius.circular(10)
                            ),
                            child: Column(
                              children: [
                                Image.asset(
                                    "images/cards/lvl_up.png",
                                  height: 30,
                                  width: 30,
                                ),
                                SizedBox(height: 5),
                                Text(
                                  "LVL UP!",
                                  style: TextStyle(
                                      color: Color(0xffFF6C98),
                                      fontWeight: FontWeight.w500,
                                      fontSize: 8
                                  ),
                                ),
                                SizedBox(height: 5),
                                Text(
                                  "Вы сильно преуспели и повысились в должности",
                                  style: TextStyle(
                                      color: Color.fromRGBO(255, 108, 152, 0.5),
                                      fontWeight: FontWeight.w500,
                                      fontSize: 6
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(width: 10),
                          Container(
                            height: 90,
                            width: 104,
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10)
                            ),
                            child: Column(
                              children: [
                                Image.asset(
                                    "images/cards/moneyman.png",
                                  height: 30,
                                  width: 30,
                                ),
                                SizedBox(height: 5),
                                Text(
                                  "Moneyman",
                                  style: TextStyle(
                                      color: Color(0xff6A40AF),
                                      fontWeight: FontWeight.w500,
                                      fontSize: 8
                                  ),
                                ),
                                SizedBox(height: 5),
                                Text(
                                  "Вы приносите компании много денег",
                                  style: TextStyle(
                                      color: Color.fromRGBO(106, 64, 175, 0.5),
                                      fontWeight: FontWeight.w500,
                                      fontSize: 6
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(width: 10),
                          Container(
                            height: 90,
                            width: 104,
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              color: Color(0xffF0F1FE),
                                borderRadius: BorderRadius.circular(10)
                            ),
                            child: Column(
                              children: [
                                Image.asset(
                                    "images/recommendations_page/keyboard_simulator.png",
                                  height: 30,
                                  width: 30,
                                ),
                                SizedBox(height: 5),
                                Text(
                                  "Иду к цели!",
                                  style: TextStyle(
                                      color: Color(0xff6A40AF),
                                      fontWeight: FontWeight.w500,
                                      fontSize: 8
                                  ),
                                ),
                                SizedBox(height: 5),
                                Text(
                                  "Выполнение плана 5 месяцев подряд",
                                  style: TextStyle(
                                      color: Color.fromRGBO(106, 64, 175, 0.5),
                                      fontWeight: FontWeight.w500,
                                      fontSize: 6
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 22),
                    ],
                  )
                ),
                Container(
                    padding: EdgeInsets.only(
                        top: 30,
                        left: 27,
                        right: 21
                    ),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(40),
                        boxShadow: [BoxShadow(color: Color.fromRGBO(27, 11, 71, 0.1))]
                    ),
                    child: Column(
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(width: 3),
                            Image.asset(
                                "assets/avatars/Nikita.png",
                                height: 100,
                                width: 100
                            ),
                            SizedBox(width: 25),
                            Column(
                              children: [
                                Text(
                                  "Никита Аверкин",
                                  style: TextStyle(
                                      color: Color(0xff192AA7),
                                      fontSize: 10,
                                      fontWeight: FontWeight.w500
                                  ),
                                ),
                                SizedBox(height: 13),
                                Row(
                                  children: [
                                    Image.asset(
                                        "assets/trophy.png"
                                    ),
                                    SizedBox(width: 10),
                                    Text(
                                      "93 уровень",
                                      style: TextStyle(
                                          color: Color(0xffFFC250),
                                          fontWeight: FontWeight.w500,
                                          fontSize: 5
                                      ),
                                    )
                                  ],
                                )
                              ],
                            )
                          ],
                        ),
                        SizedBox(height: 20),
                        Row(
                          children: [
                            Container(
                              height: 90,
                              width: 104,
                              padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  color: Color(0xffFDFDF1),
                                  borderRadius: BorderRadius.circular(10)
                              ),
                              child: Column(
                                children: [
                                  Image.asset(
                                    "images/cards/in_the_last_moment.png",
                                    height: 30,
                                    width: 30,
                                  ),
                                  SizedBox(height: 5),
                                  Text(
                                    "В последний момент",
                                    style: TextStyle(
                                        color: Color(0xffFF7337),
                                        fontWeight: FontWeight.w500,
                                        fontSize: 8
                                    ),
                                  ),
                                  SizedBox(height: 5),
                                  Text(
                                    "В последний момент",
                                    style: TextStyle(
                                        color: Color.fromRGBO(255, 115, 55, 0.5),
                                        fontWeight: FontWeight.w500,
                                        fontSize: 6
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(width: 10),
                            Container(
                              height: 90,
                              width: 104,
                              padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  color: Color(0xffFDF0FB),
                                  borderRadius: BorderRadius.circular(10)
                              ),
                              child: Column(
                                children: [
                                  Image.asset(
                                    "images/cards/lvl_up.png",
                                    height: 30,
                                    width: 30,
                                  ),
                                  SizedBox(height: 5),
                                  Text(
                                    "LVL UP!",
                                    style: TextStyle(
                                        color: Color(0xffFF6C98),
                                        fontWeight: FontWeight.w500,
                                        fontSize: 8
                                    ),
                                  ),
                                  SizedBox(height: 5),
                                  Text(
                                    "Вы сильно преуспели и повысились в должности",
                                    style: TextStyle(
                                        color: Color.fromRGBO(255, 108, 152, 0.5),
                                        fontWeight: FontWeight.w500,
                                        fontSize: 6
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(width: 10),
                            Container(
                              height: 90,
                              width: 104,
                              padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10)
                              ),
                              child: Column(
                                children: [
                                  Image.asset(
                                    "images/cards/moneyman.png",
                                    height: 30,
                                    width: 30,
                                  ),
                                  SizedBox(height: 5),
                                  Text(
                                    "Moneyman",
                                    style: TextStyle(
                                        color: Color(0xff6A40AF),
                                        fontWeight: FontWeight.w500,
                                        fontSize: 8
                                    ),
                                  ),
                                  SizedBox(height: 5),
                                  Text(
                                    "Вы приносите компании много денег",
                                    style: TextStyle(
                                        color: Color.fromRGBO(106, 64, 175, 0.5),
                                        fontWeight: FontWeight.w500,
                                        fontSize: 6
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(width: 10),
                            Container(
                              height: 90,
                              width: 104,
                              padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  color: Color(0xffF0F1FE),
                                  borderRadius: BorderRadius.circular(10)
                              ),
                              child: Column(
                                children: [
                                  Image.asset(
                                    "images/recommendations_page/keyboard_simulator.png",
                                    height: 30,
                                    width: 30,
                                  ),
                                  SizedBox(height: 5),
                                  Text(
                                    "Иду к цели!",
                                    style: TextStyle(
                                        color: Color(0xff6A40AF),
                                        fontWeight: FontWeight.w500,
                                        fontSize: 8
                                    ),
                                  ),
                                  SizedBox(height: 5),
                                  Text(
                                    "Выполнение плана 5 месяцев подряд",
                                    style: TextStyle(
                                        color: Color.fromRGBO(106, 64, 175, 0.5),
                                        fontWeight: FontWeight.w500,
                                        fontSize: 6
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: 22),
                      ],
                    )
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

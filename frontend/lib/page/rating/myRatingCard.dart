import 'package:flutter/material.dart';
import 'package:frontend/api/client.dart';
import 'package:frontend/api/schema/profile.dart';
import 'package:frontend/constants.dart';
import 'package:frontend/global.dart';
import 'package:frontend/page/rating/miniCards.dart';
import 'package:websafe_svg/websafe_svg.dart';

class MyRating extends StatefulWidget {
  @override
  _MyRatingState createState() => _MyRatingState();
}

class _MyRatingState extends State<MyRating> {
  Profile? _profile;

  @override
  void initState() {
    super.initState();
    client.getProfile(Prefs.telegram!).then((Profile value) {
      setState(() {
        _profile = value;
      });
    });
  }

  /// Рейтинг пользователя
  @override
  Widget build(BuildContext context) {
    if (_profile == null) return Container();
    return Container(
      margin: EdgeInsets.only(top: 15, left: 20, right: 20),
      height: 270,
      decoration: BoxDecoration(
        color: white,
        borderRadius: BorderRadius.circular(30),
        boxShadow: [BoxShadow(color: shadow, blurRadius: 15)],
      ),
      child: Container(
        padding: EdgeInsets.all(40),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Expanded(
              flex: 1,
              child: Row(
                children: [
                  Container(
                    width: 100,
                    height: 100,
                    child: Image.asset(
                      'avatars/1.png',
                      scale: 0.8,
                    ),
                  ),
                  SizedBox(width: 20),
                  Expanded(
                    flex: 1,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(
                              _profile!.fio,
                              style: TextStyle(color: blueText, fontSize: 28),
                            ),
                            Spacer(),
                            Text(
                              '65%',
                              style: TextStyle(color: blueText, fontSize: 28),
                            ),
                            Container(
                              width: 170,
                              height: 15,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                                border: Border.all(color: indicator),
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                                child: LinearProgressIndicator(
                                  value: 0.65,
                                  backgroundColor: white,
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                    blueText,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(width: 10),
                            WebsafeSvg.asset('icons/coins.svg'),
                            Text(
                              (_profile!.balance != null
                                      ? _profile!.balance
                                      : 'N/A')
                                  .toString(),
                              style: TextStyle(color: blueText, fontSize: 18),
                            )
                          ],
                        ),
                        SizedBox(height: 20),
                        Row(
                          children: [
                            Icon(
                              Icons.arrow_circle_up,
                              color: Colors.amber,
                              size: 30,
                            ),
                            SizedBox(width: 20),
                            Text(
                              '93 уровень',
                              style: TextStyle(color: Colors.amber),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 10),
            Expanded(
              flex: 1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    margin: EdgeInsets.only(
                      top: 15,
                    ),
                    width: 100,
                    height: 100,
                    child: WebsafeSvg.asset('images/cards/lvl_up.svg'),
                  ),
                  for (var i = 0; i < 6; i++) MiniCard(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

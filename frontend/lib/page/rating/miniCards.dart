import 'package:flutter/material.dart';
import 'package:frontend/constants.dart';

class MiniCard extends StatefulWidget {
  @override
  _MiniCardState createState() => _MiniCardState();
}

class _MiniCardState extends State<MiniCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: 15,
        left: 20,
        right: 20,
      ),
      width: 100,
      height: 100,
      decoration: BoxDecoration(
        color: white,
        borderRadius: BorderRadius.circular(15),
        boxShadow: [
          BoxShadow(color: shadow, blurRadius: 15),
        ],
      ),
      child: Container(),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:frontend/constants.dart';
import 'package:websafe_svg/websafe_svg.dart';

class MyTasks extends StatefulWidget {
  @override
  _MyTasksState createState() => _MyTasksState();
}

class _MyTasksState extends State<MyTasks> {
  /// Карточка Задания пользователя
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 310,
      decoration: BoxDecoration(
        color: white,
        borderRadius: BorderRadius.circular(30),
        boxShadow: [
          BoxShadow(color: shadow, blurRadius: 15),
        ],
      ),
      child: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(30),
        child: Stack(
          alignment: Alignment.center,
          children: [
            Positioned(
              left: 0,
              child: Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(
                  top: 22,
                  left: 11,
                  bottom: 58
                ),
                decoration: BoxDecoration(
                  color: Color(0xFFF0F1FE),
                  borderRadius: BorderRadius.circular(30),
                  boxShadow: [
                    BoxShadow(color: shadow, blurRadius: 10),
                  ],
                ),
                child: Image.asset(
                  "assets/images/recommendations_page/keyboard_simulator.png",
                  height: 155,
                  width: 288,
                ),
              ),
            ),
            Positioned(
              left: 132,
              child: Container(
                height: 230,
                width: 561,
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(
                  top: 57,
                  left: 50,
                  bottom: 47
                ),
                decoration: BoxDecoration(
                  color: Color(0xFFFDFDF1),
                  borderRadius: BorderRadius.circular(30),
                  boxShadow: [
                    BoxShadow(color: shadow, blurRadius: 10),
                  ],
                ),
                child: Image.asset(
                  "assets/images/recommendations_page/personal_income_tax.png",
                  height: 131,
                  width: 238,
                ),
              ),
            ),
            Positioned(
              left: 264,
              right: 0,
              child: Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(
                    top: 36,
                  left: 36,
                  right: 25,
                  bottom: 36
                ),
                decoration: BoxDecoration(
                  color: Color(0xFFFDF0FB),
                  borderRadius: BorderRadius.circular(30),
                  boxShadow: [
                    BoxShadow(color: shadow, blurRadius: 10),
                  ],
                ),
                child: Row(
                  children: [
                    Image.asset(
                      'images/cards/machine-killer.png',
                      width: 167,
                      height: 163,
                    ),
                    SizedBox(width: 30),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            'Машина - убийца',
                            style: TextStyle(
                                color: Color(0xFFFF6C98), fontSize: 18),
                          ),
                          Text(
                            'Выполнил много заданий? Думаешь, что уже можешь осилить все? Что ж, тогда следующее задание для тебя! Нужно верифицировать 10 заявок 2-НДФЛ.\nЗадание не простое, но и награда будет соответствующей!',
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                color: Color(0x88FF6C98), fontSize: 14),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getPanel(double width, Color color, double left) => Positioned(
        left: left,
        right: 0,
        child: Container(
          alignment: Alignment.centerRight,
          height: 230,
          width: width,
          decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.circular(30),
            boxShadow: [
              BoxShadow(color: shadow, blurRadius: 10),
            ],
          ),
        ),
      );
}

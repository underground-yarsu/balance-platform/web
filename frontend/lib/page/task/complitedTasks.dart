import 'package:flutter/material.dart';
import 'package:frontend/constants.dart';
import 'package:websafe_svg/websafe_svg.dart';

class ComplitedTasks extends StatefulWidget {
  @override
  _ComplitedTasksState createState() => _ComplitedTasksState();
}

/// Завершенные задания
class _ComplitedTasksState extends State<ComplitedTasks> {
  var complitedTasks = [
    'agr',
    'going',
    'good_idea',
    'last_time',
    'lvl_up',
    'money_man',
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 450,
      decoration: BoxDecoration(
        color: white,
        borderRadius: BorderRadius.circular(30),
        boxShadow: [
          BoxShadow(color: shadow, blurRadius: 15),
        ],
      ),
      child: Container(
        padding: EdgeInsets.all(30),
        child: Container(
          height: 230,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  for (var i = 0; i < 4; i++)
                    WebsafeSvg.asset(
                      'images/cards/${complitedTasks[i]}.svg',
                      height: 180,
                      width: 130,
                    )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  for (var i = 5; i > 1; i--)
                    WebsafeSvg.asset(
                      'images/cards/${complitedTasks[i]}.svg',
                      height: 180,
                      width: 130,
                    )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

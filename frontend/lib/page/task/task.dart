import 'package:flutter/material.dart';
import 'package:frontend/constants.dart';
import 'package:frontend/page/task/complitedTasks.dart';
import 'package:frontend/page/task/taskCard.dart';

class TaskPage extends StatefulWidget {
  @override
  _TaskPageState createState() => _TaskPageState();
}

class _TaskPageState extends State<TaskPage> {
  /// Вкладка Задачи
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      alignment: Alignment.centerRight,
      padding: EdgeInsets.only(top: 123, left: 84, right: 76),
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                'Задачи',
                style: TextStyle(color: blueText, fontSize: 28),
              ),
            ),
            SizedBox(height: 16),
            MyTasks(),
            SizedBox(height: 30),
            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                'Завершенные задачи',
                style: TextStyle(color: blueText, fontSize: 28),
              ),
            ),
            SizedBox(height: 16),
            ComplitedTasks(),
          ],
        ),
      ),
    );
  }
}

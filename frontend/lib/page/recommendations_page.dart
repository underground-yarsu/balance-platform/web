import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:websafe_svg/websafe_svg.dart';

/// Личный кабинет
class RecommendationsPage extends StatefulWidget {
  @override
  _RecommendationsPageState createState() => _RecommendationsPageState();
}

class _RecommendationsPageState extends State<RecommendationsPage> {

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
          top: 123,
          left: 84,
          right: 84
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
              "Советы",
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 28,
                  color: Color(0xff192AA7)
              )
          ),
          SizedBox(height: 16),
          Container(
            padding: EdgeInsets.only(
                top: 40,
                left: 50,
                right: 50
            ),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(40),
                boxShadow: [BoxShadow(color: Color.fromRGBO(27, 11, 71, 0.1))]
            ),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(
                      left: 60,
                      top: 20,
                      bottom: 21
                  ),
                  decoration: BoxDecoration(
                      color: Color(0xfff0f1fe),
                      borderRadius: BorderRadius.circular(40)
                  ),
                  child: Row(
                    children: [
                      Image.asset(
                        "images/recommendations_page/keyboard_simulator.png",
                        height: 100,
                        width: 147.16,
                      ),
                      SizedBox(width: 45.84),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                              "Клавиатурный тренажер",
                              style: TextStyle(
                                  color: Color(0xff6A40AF),
                                  fontWeight: FontWeight.w500,
                                  fontSize: 22
                              )
                          ),
                          SizedBox(height: 10),
                          Text(
                              'Ты совершаешь много ошибок при наборе текста!',
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                  color: Color.fromRGBO(106, 64, 175, 0.5)
                              )
                          ),
                          RichText(
                            text: TextSpan(
                              children: <TextSpan>[
                                TextSpan(
                                    text: 'Попробуй использовать ',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 14,
                                        color: Color.fromRGBO(106, 64, 175, 0.5)
                                    )
                                ),
                                TextSpan(
                                    text: 'https://www.ratatype.ru/',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 14,
                                        color: Color.fromRGBO(106, 64, 175, 1)
                                    )
                                ),
                              ],
                            ),
                          ),
                          Text(
                              'Он поможет!',
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                  color: Color.fromRGBO(106, 64, 175, 0.5)
                              )
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(height: 30),
                Container(
                  padding: EdgeInsets.only(
                      left: 60,
                      top: 20,
                      bottom: 21
                  ),
                  decoration: BoxDecoration(
                      color: Color(0xfffdfdf1),
                      borderRadius: BorderRadius.circular(40)
                  ),
                  child: Row(
                    children: [
                      Image.asset(
                        "images/recommendations_page/personal_income_tax.png",
                        height: 100,
                        width: 147.16,
                      ),
                      SizedBox(width: 45.84),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                              "2-НДФЛ",
                              style: TextStyle(
                                  color: Color(0xffff7337),
                                  fontWeight: FontWeight.w500,
                                  fontSize: 22
                              )
                          ),
                          SizedBox(height: 10),
                          Text(
                              'Справка 2-НДФЛ - это не легко. Попробуй еще раз изучить эту тему в базе знаний',
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                  color: Color.fromRGBO(255, 115, 55, 0.5)
                              )
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

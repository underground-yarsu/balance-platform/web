import 'package:flutter/material.dart';
import 'package:frontend/constants.dart';

class Answer extends StatelessWidget {
  const Answer({
    required this.title,
    required this.onChangeAnswer,
    Key? key,
  }) : super(key: key);
  final String title;
  final Function onChangeAnswer;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onChangeAnswer(context),
      child: Container(
        margin: EdgeInsets.all(7),
        width: 350,
        height: 50,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: white,
        ),
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(horizontal: 30),
        child: Text(
          title,
          style: TextStyle(
            fontStyle: FontStyle.normal,
            fontSize: 14,
            color: blueText,
          ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:frontend/api/client.dart';
import 'package:frontend/components/authBack.dart';
import 'package:frontend/constants.dart';
import 'package:frontend/models/questionsData.dart';
import 'package:frontend/page/profileView.dart';

import 'answer.dart';

class StartQuestions extends StatefulWidget {
  StartQuestions({Key? key}) : super(key: key);

  @override
  _StartQuestionsState createState() => _StartQuestionsState();
}

class _StartQuestionsState extends State<StartQuestions> {
  final QuestionsData data = QuestionsData();
  void clearState() => setState(() {
        questionIndex = 0;
        countResult = 0;
      });

  void onChangeAnswer(context) {
    countResult++;
    questionIndex++;
    if (questionIndex > 4) {
      client.updStatus(1);
      Navigator.push<void>(
        context,
        MaterialPageRoute<void>(
          builder: (BuildContext context) => ProfileView(),
        ),
      );
    } else {
      setState(() {});
    }
  }

  int countResult = 0;
  int questionIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AuthPage(
        width: 610,
        height: 610,
        child: Column(
          children: [
            SizedBox(height: MediaQuery.of(context).size.height * 0.05),
            Text(
              'Пара вопросов, чтобы начать',
              style: TextStyle(fontSize: 32, color: text),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.02),
            Text(
              "Ответьте на несколько вопросв,\nчтобы мы узнали Ваш тип мотивации",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontStyle: FontStyle.normal, fontSize: 14, color: semiText),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.05),
            Container(
              width: 450,
              height: 400,
              decoration: BoxDecoration(
                color: white.withOpacity(0.6),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Column(
                children: [
                  SizedBox(height: MediaQuery.of(context).size.height * 0.03),
                  Text('${questionIndex + 1}/5',
                      style: TextStyle(
                          fontStyle: FontStyle.normal,
                          fontSize: 16,
                          color: text)),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                  Container(
                    width: 370,
                    child: Text(
                      data.questions[questionIndex].title,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      maxLines: 3,
                      style: TextStyle(
                        color: text,
                        fontSize: 18,
                      ),
                    ),
                  ),
                  ...data.questions[questionIndex].answers
                      .map(
                        (value) => GestureDetector(
                          onTap: () => onChangeAnswer(context),
                          child: Answer(
                            title: value['answer'],
                            onChangeAnswer: onChangeAnswer,
                          ),
                        ),
                      )
                      .toList(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

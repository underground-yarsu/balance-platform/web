import 'package:flutter/material.dart';
import 'package:frontend/components/battleDialog.dart';
import 'package:frontend/components/buttons.dart';
import 'package:frontend/constants.dart';

/// Карточка соревнования
class ChallengeCard extends StatelessWidget {
  final String commandName;
  final String avatar;
  ChallengeCard({required this.commandName, required this.avatar});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(bottom: 14),
      child: Container(
        height: 102,
        width: 539,
        padding: EdgeInsets.symmetric(
          horizontal: 30,
          vertical: 16
        ),
        decoration: BoxDecoration(
            color: white,
            borderRadius: BorderRadius.circular(30),
            boxShadow: [BoxShadow(color: shadow, blurRadius: 15)]
        ),
        child: Row(
          children: [
            Image.asset('avatars/$avatar.png'),
            SizedBox(width: 20),
            Text(
              commandName,
              style: TextStyle(color: blueText, fontSize: 18),
            ),
            Spacer(),
            MyOutlinedButton(
                onTap: () => showDialog(
                    context: context,
                    builder: (BuildContext context) => BattleDialog(context)),
                text: 'Бросить вызов'
            )
          ],
        ),
      ),
    );
  }
}

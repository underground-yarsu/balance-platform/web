import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

/// Страница со статистикой
class StatsPage extends StatefulWidget {
  const StatsPage({Key? key}) : super(key: key);

  @override
  _StatsPageState createState() => _StatsPageState();
}

class Pair {
  Pair(this.first, this.second);
  final int first;
  final double second;
}

class _StatsPageState extends State<StatsPage> {
  Widget _buildCard(Widget child) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(40),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            spreadRadius: 0,
            blurRadius: 20,
          ),
        ],
      ),
      child: child,
    );
  }

  Widget _buildMiniCardContent(int i) {
    Widget __buildMiniCardContent(String left, String right) {
      return Padding(
        padding: const EdgeInsets.all(18.0),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: Text(
                    left,
                    style: TextStyle(
                      fontSize: 16,
                    ),
                  ),
                ),
                Text(
                  right,
                  style: TextStyle(
                    fontSize: 16,
                    color: Color(0xFF855CF8),
                  ),
                ),
              ],
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(top: 18),
                child: Image.asset(
                  "assets/plot.png",
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ],
        ),
      );
    }

    switch (i) {
      case 0:
        return __buildMiniCardContent("Время обработки заявки", "2,5 мин");
      case 1:
        return __buildMiniCardContent("Кол-во заявок в час", "32");
      case 2:
        return __buildMiniCardContent("Количество заявок", "1200");
      case 3:
        return __buildMiniCardContent("Скорость нахождения ошибок", "2 мин");
      case 4:
        return __buildMiniCardContent("Количество ошибок", "3");
      case 5:
        return __buildMiniCardContent("Скорость печати", "3 слов/мин");
      default:
        return Placeholder();
    }
  }

  Widget _buildMiniCard(Widget? child) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(14),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Color(0xFF1B0B47).withOpacity(0.1),
            spreadRadius: 0,
            blurRadius: 20,
          ),
        ],
      ),
      child: child,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(
            left: 134,
            top: 122,
          ),
          child: Text(
            "Статистика",
            style: TextStyle(
              color: Color(0xFF192AA7),
              fontSize: 28,
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(
              top: 16,
              left: 84,
              bottom: 62,
              right: 76,
            ),
            child: _buildCard(
              Column(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(
                        top: 50,
                        bottom: 25,
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                left: 50,
                                right: 25,
                              ),
                              child: _buildMiniCard(_buildMiniCardContent(0)),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                left: 25,
                                right: 25,
                              ),
                              child: _buildMiniCard(_buildMiniCardContent(1)),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                left: 25,
                                right: 50,
                              ),
                              child: _buildMiniCard(_buildMiniCardContent(2)),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(
                        top: 25,
                        bottom: 50,
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                left: 50,
                                right: 25,
                              ),
                              child: _buildMiniCard(_buildMiniCardContent(3)),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                left: 25,
                                right: 25,
                              ),
                              child: _buildMiniCard(_buildMiniCardContent(4)),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                left: 25,
                                right: 50,
                              ),
                              child: _buildMiniCard(_buildMiniCardContent(5)),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}

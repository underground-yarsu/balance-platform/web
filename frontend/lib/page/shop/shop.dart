import 'package:flutter/material.dart';
import 'package:frontend/components/buttons.dart';
import 'package:websafe_svg/websafe_svg.dart';

class ShopPage extends StatefulWidget {
  @override
  _ShopPageState createState() => _ShopPageState();
}

class _ShopPageState extends State<ShopPage> {
  Widget _buildMiniCardContent(int i) {
    Widget __buildMiniCardContent(String name, String coast) {
      return Padding(
        padding: const EdgeInsets.all(18.0),
        child: Row(
          children: [
            Container(
              child: Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Image.asset(
                  "assets/images/shop/shop$i.png",
                ),
              ),
            ),
            SizedBox(width: 20),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        name,
                        style: TextStyle(
                          fontSize: 24,
                        ),
                      ),
                      Row(
                        children: [
                          Text(
                            coast,
                            style: TextStyle(
                              fontSize: 22,
                              color: Color(0xFF855CF8),
                            ),
                          ),
                          SizedBox(width: 10),
                          WebsafeSvg.asset('icons/coins.svg',
                              height: 20, width: 20),
                        ],
                      ),
                    ],
                  ),
                ),
                MyButton(text: 'Купить', onTap: () {}),
              ],
            ),
          ],
        ),
      );
    }

    switch (i) {
      case 1:
        return __buildMiniCardContent("Кофта \n'Future'", "1200");
      case 2:
        return __buildMiniCardContent("Конфеты линейки\n'Не сахар'", "1000");
      case 3:
        return __buildMiniCardContent("Набор мерча \nкомпании", "1200");
      case 4:
        return __buildMiniCardContent("Футболка \n'Game hunters'", "1500");
      default:
        return Placeholder();
    }
  }

  Widget _buildMiniCard(Widget? child) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(14),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Color(0xFF1B0B47).withOpacity(0.1),
            spreadRadius: 0,
            blurRadius: 20,
          ),
        ],
      ),
      child: child,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(
            left: 134,
            top: 122,
          ),
          child: Text(
            "Магазин",
            style: TextStyle(
              color: Color(0xFF192AA7),
              fontSize: 28,
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(
              top: 16,
              left: 84,
              bottom: 62,
              right: 76,
            ),
            child: Column(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(
                      top: 50,
                      bottom: 25,
                    ),
                    child: Row(
                      children: [
                        Container(
                          width: 500,
                          height: 500,
                          child: Padding(
                            padding: const EdgeInsets.only(
                              left: 50,
                              right: 25,
                            ),
                            child: _buildMiniCard(_buildMiniCardContent(1)),
                          ),
                        ),
                        Container(
                          width: 500,
                          height: 500,
                          child: Padding(
                            padding: const EdgeInsets.only(
                              left: 25,
                              right: 25,
                            ),
                            child: _buildMiniCard(_buildMiniCardContent(2)),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(
                      top: 25,
                      bottom: 50,
                    ),
                    child: Row(
                      children: [
                        Container(
                          width: 500,
                          height: 500,
                          child: Padding(
                            padding: const EdgeInsets.only(
                              left: 50,
                              right: 25,
                            ),
                            child: _buildMiniCard(_buildMiniCardContent(3)),
                          ),
                        ),
                        Container(
                          width: 500,
                          height: 500,
                          child: Padding(
                            padding: const EdgeInsets.only(
                              left: 25,
                              right: 25,
                            ),
                            child: _buildMiniCard(_buildMiniCardContent(4)),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

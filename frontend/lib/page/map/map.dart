import 'package:flutter/material.dart';

/// Вкладка карта
class MapPage extends StatefulWidget {
  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  @override
  Widget build(BuildContext context) {
    return Image.asset(
        'assets/images/map.png',
        fit: BoxFit.none
    );
  }
}

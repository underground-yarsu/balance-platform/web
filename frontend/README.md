# Frontend

## Сборка

1. Установка [flutter](https://flutter.dev/docs/get-started/install)
2. Получение зависимостей
   
   `flutter pub get`
3. Запуск кодогенерации
   
   `flutter pub run build_runner build`
4. Сборка под web
   
   `flutter build web --release`

   или запуск
   
   `flutter run --release`

## Скриншоты

![img1](img/1.png)
![img3](img/3.png)
![img9](img/9.png)